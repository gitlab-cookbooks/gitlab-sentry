require 'spec_helper'
require 'chef-vault'

statsd_mappings_content = <<-'MAPPINGS'
---
defaults:
  buckets:
  - 0.005
  - 0.01
  - 0.025
  - 0.05
  - 0.1
  - 0.25
  - 0.5
  - 1.0
  - 2.5
  - 10.0
  - 25.0
  match_type: glob
  timer_type: histogram
mappings:
- match: sentry.client-api.all-versions.responses.*
  action: drop
  name: dropped
- match: sentry.client-api.all-versions.requests
  action: drop
  name: dropped
- match: sentry\.client-api\.(.+)\.responses\.(\dxx)
  name: sentry_client_api_responses_total
  match_type: regex
  action: drop
- match: sentry\.client-api\.(.+)\.responses\.(\d{3})
  name: sentry_client_api_responses_total
  match_type: regex
  labels:
    api_version: "$1"
    status: "$2"
- match: sentry.jobs.all.*
  action: drop
  name: dropped
- match: sentry.jobs.delay
  name: sentry_jobs_delay_seconds
- match: sentry.jobs.duration
  name: sentry_jobs_duration_seconds
- match: sentry.jobs.memory_change
  name: sentry_jobs_memory_bytes
- match: sentry.jobs.*
  name: sentry_jobs_${1}_total
- match: sentry.view.duration
  name: sentry_view_duration_seconds
- match: sentry.view.response
  action: drop
  name: dropped
- match: sentry.sourcemaps.release_file
  name: sentry_sourcemaps_release_file_seconds
- match: sentry.events.latency
  name: sentry_events_latency_seconds
- match: sentry.events.processed
  action: drop
  name: dropped
- match: sentry.events.processed.*
  name: sentry_events_processed_total
  labels:
    event: "$1"
- match: sentry.events.blacklisted
  name: sentry_events_blacklisted_total
- match: sentry.events.latency
  name: sentry_events_latency_seconds
MAPPINGS

describe 'gitlab-sentry::statsd_exporter' do
  context 'default execution' do
    cached(:chef_run) do
      ChefSpec::SoloRunner.new do |node|
      end.converge('consul::default', described_recipe)
    end

    it 'converges successfully' do
      expect { chef_run }.to_not raise_error
    end

    it 'creates a statsd mapping config' do
      expect(chef_run).to render_file('/opt/prometheus/statsd_exporter/statsd_mappings.yml').with_content(statsd_mappings_content)
    end
  end
end
