#gitlab-sentry CHANGELOG

This file is used to list changes made in each version of the gitlab-sentry cookbook.

##0.1.2
- Jeroen Nijhof - Upgrade sentry to 8.12.0

##0.1.1
- Jeroen Nijhof - Use chef_nginx

##0.1.0
- Jeroen Nijhof - Initial release of gitlab-sentry

