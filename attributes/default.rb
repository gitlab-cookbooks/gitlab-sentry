default['gitlab-sentry']['secrets']['backend'] = 'gkms'
default['gitlab-sentry']['secrets']['path'] = {
  'path' => 'gitlab-ops-secrets/gitlab-sentry',
  'item' => 'ops.enc',
}
default['gitlab-sentry']['secrets']['key'] = {
  'ring' => 'gitlab-secrets',
  'key' => 'ops',
  'location' => 'global',
}

default['gitlab-sentry']['fqdn'] = node['fqdn']
default['gitlab-sentry']['config_path'] = '/etc/sentry'
default['gitlab-sentry']['install_path'] = '/usr/share/nginx/sentry'
default['gitlab-sentry']['admin_email'] = 'ops-contact+sentry@gitlab.com'
default['gitlab-sentry']['ssl_certificate'] = 'override_attribute in role!'
default['gitlab-sentry']['ssl_key'] = 'override_attribute in vault!'
default['gitlab-sentry']['secret_key'] = 'override_attribute in vault!'
default['gitlab-sentry']['database']['host'] = '127.0.0.1'
default['gitlab-sentry']['database']['port'] = '5432'
default['gitlab-sentry']['database']['name'] = 'sentry'
default['gitlab-sentry']['database']['user'] = 'sentry'
default['gitlab-sentry']['database']['password'] = 'override_attribute in vault!'
default['gitlab-sentry']['redis']['host'] = '127.0.0.1'
default['gitlab-sentry']['redis']['port'] = '6379'
default['gitlab-sentry']['smtp']['host'] = 'localhost'
default['gitlab-sentry']['smtp']['port'] = 25
default['gitlab-sentry']['smtp']['user'] = ''
default['gitlab-sentry']['smtp']['password'] = ''
default['gitlab-sentry']['smtp']['tls'] = 'False'
default['gitlab-sentry']['smtp']['mail_from'] = 'root@localhost'
default['gitlab-sentry']['gitlab']['app_id'] = 'override_attribute in vault!'
default['gitlab-sentry']['gitlab']['app_secret'] = 'override_attribute in vault!'
default['gitlab-sentry']['gitlab']['base_domain'] = 'dev.gitlab.org'
