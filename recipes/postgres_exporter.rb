# Configure the postgres_exporter for sentry

include_recipe 'gitlab-exporters::default'

env = node.chef_environment
node.default['postgres_exporter']['secrets'] = {
  backend: 'gkms',
  path: {
    path: "gitlab-#{env}-secrets/gitlab-sentry",
    item: "#{env}.enc",
  },
  key: {
    ring: 'gitlab-secrets',
    key: env,
    location: 'global',
  },
} if !node['cloud'].nil? && node['cloud']['provider'] == 'gce'

node.default['postgres_exporter']['database'] = 'sentry'

include_recipe 'gitlab-exporters::postgres_exporter'
