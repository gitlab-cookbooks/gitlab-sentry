# Configure the statsd_exporter for sentry

include_recipe 'gitlab-exporters::default'

node.default['statsd_exporter']['mappings'] += [
  {
    match: 'sentry.client-api.all-versions.responses.*',
    action: 'drop',
    name: 'dropped',
  },
  {
    match: 'sentry.client-api.all-versions.requests',
    action: 'drop',
    name: 'dropped',
  },
  {
    match: 'sentry\\.client-api\\.(.+)\\.responses\\.(\\dxx)',
    name: 'sentry_client_api_responses_total',
    match_type: 'regex',
    action: 'drop',
  },
  {
    match: 'sentry\\.client-api\\.(.+)\\.responses\\.(\\d{3})',
    name: 'sentry_client_api_responses_total',
    match_type: 'regex',
    labels: {
      api_version: '$1',
      status: '$2',
    },
  },
  {
    match: 'sentry.jobs.all.*',
    action: 'drop',
    name: 'dropped',
  },
  {
    match: 'sentry.jobs.delay',
    name: 'sentry_jobs_delay_seconds',
  },
  {
    match: 'sentry.jobs.duration',
    name: 'sentry_jobs_duration_seconds',
  },
  {
    match: 'sentry.jobs.memory_change',
    name: 'sentry_jobs_memory_bytes',
  },
  {
    match: 'sentry.jobs.*',
    name: 'sentry_jobs_${1}_total',
  },
  {
    match: 'sentry.view.duration',
    name: 'sentry_view_duration_seconds',
  },
  {
    match: 'sentry.view.response',
    action: 'drop',
    name: 'dropped',
  },
  {
    match: 'sentry.sourcemaps.release_file',
    name: 'sentry_sourcemaps_release_file_seconds',
  },
  {
    match: 'sentry.events.latency',
    name: 'sentry_events_latency_seconds',
  },
  {
    match: 'sentry.events.processed',
    action: 'drop',
    name: 'dropped',
  },
  {
    match: 'sentry.events.processed.*',
    name: 'sentry_events_processed_total',
    labels: {
      event: '$1',
    },
  },
  {
    match: 'sentry.events.blacklisted',
    name: 'sentry_events_blacklisted_total',
  },
  {
    match: 'sentry.events.latency',
    name: 'sentry_events_latency_seconds',
  },
]

include_recipe 'gitlab-exporters::statsd_exporter'
