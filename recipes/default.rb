#
# Cookbook:: gitlab-sentry
# Recipe:: default
#
# Copyright:: 2016, GitLab Inc.
#
# All rights reserved - Do Not Redistribute
#
include_recipe 'postgresql::server'
include_recipe 'gitlab-sentry::statsd_exporter'
include_recipe 'gitlab-sentry::postgres_exporter'

env = node.chef_environment
node.default['gitlab-sentry']['secrets'] = {
  backend: 'gkms',
  path: {
    path: "gitlab-#{env}-secrets/gitlab-sentry",
    item: "#{env}.enc",
  },
  key: {
    ring: 'gitlab-secrets',
    key: env,
    location: 'global',
  },
} if !node['cloud'].nil? && node['cloud']['provider'] == 'gce'

# Fetch secrets
sentry_secrets = get_secrets(node['gitlab-sentry']['secrets']['backend'],
  node['gitlab-sentry']['secrets']['path'],
  node['gitlab-sentry']['secrets']['key'])

sentry_conf = Chef::Mixin::DeepMerge.deep_merge(sentry_secrets['gitlab-sentry'], node['gitlab-sentry'].to_hash)

directory sentry_conf['config_path']

## nginx
nginx_install 'default' do
  conf_cookbook 'gitlab-sentry'
  conf_template 'nginx-sentry.conf.erb'
  source 'repo'
  default_site_enabled false
end

# Workaround nginx_install not exposing the service resource.
service 'nginx' do
  supports status: true, restart: true, reload: true
  action   [:start, :enable]
end

file "/etc/ssl/#{sentry_conf['fqdn']}.crt" do
  content sentry_conf['ssl_certificate']
  owner 'root'
  group 'root'
  mode '644'
  notifies :restart, 'service[nginx]', :delayed
end

file "/etc/ssl/#{sentry_conf['fqdn']}.key" do
  content sentry_conf['ssl_key']
  owner 'root'
  group 'root'
  mode '600'
  notifies :restart, 'service[nginx]', :delayed
end

site_vars = {
  fqdn: sentry_conf['fqdn'],
}

nginx_site 'sentry' do
  cookbook 'gitlab-sentry'
  template 'nginx-site-sentry.erb'
  variables site_vars
end

## redis
bash 'install redis' do
  code <<-EOH
    add-apt-repository ppa:chris-lea/redis-server
    apt-get update
  EOH
  not_if { ::File.exist?('/etc/apt/sources.list.d/chris-lea-ubuntu-redis-server-xenial.list') }
end

package 'redis-server'

## GCS Credentials

gcs_creds_path = File.join(sentry_conf['config_path'], 'gcs.json')
gcs_creds = Base64.decode64(sentry_secrets['gcs-creds']['json_base64'])

file gcs_creds_path do
  owner  'root'
  group  'root'
  mode   '0644'
  content gcs_creds
end

## supervisord
package 'supervisor'

service 'supervisor' do
  action [:enable, :start]
end

template '/etc/supervisor/conf.d/sentry.conf' do
  source 'supervisor-sentry-conf.erb'
  owner  'root'
  group  'root'
  mode   '0644'
  variables(
      install_path: sentry_conf['install_path'],
      gcs_creds_path: gcs_creds_path
    )
  notifies :restart, 'service[supervisor]'
end

## sentry
%w(python-setuptools python-pip python-dev python-virtualenv libxslt1-dev libxml2-dev libz-dev libffi-dev libssl-dev libpq-dev libyaml-dev libjpeg8-dev).each { |pkg| package pkg }

bash 'install sentry' do
  code <<-EOH
    pip install --upgrade pip
    pip install --upgrade virtualenv
    virtualenv #{sentry_conf['install_path']}
    #{sentry_conf['install_path']}/bin/pip install sentry
    #{sentry_conf['install_path']}/bin/pip install msgpack-python
    #{sentry_conf['install_path']}/bin/pip install google-cloud-storage
  EOH
  not_if { ::File.exist?(sentry_conf['install_path']) }
end

bash 'uninstall deprecated sentry-slack plugin' do
  code <<-EOH
    #{sentry_conf['install_path']}/bin/pip uninstall sentry-slack
  EOH
  only_if "#{sentry_conf['install_path']}/bin/pip list | grep sentry-slack"
end

bash 'uninstall deprecated sentry-gitlab plugin' do
  code <<-EOH
    #{sentry_conf['install_path']}/bin/pip uninstall sentry-gitlab
  EOH
  only_if "#{sentry_conf['install_path']}/bin/pip list | grep sentry-gitlab"
end

bash 'install sentry-plugins' do
  code <<-EOH
    #{sentry_conf['install_path']}/bin/pip install sentry-plugins
  EOH
  not_if "#{sentry_conf['install_path']}/bin/pip list | grep sentry-plugins"
end

bash 'install sentry-auth-gitlab' do
  code <<-EOH
    #{sentry_conf['install_path']}/bin/pip install https://github.com/SkyLothar/sentry-auth-gitlab/archive/v0.1.0.zip
  EOH
  not_if "#{sentry_conf['install_path']}/bin/pip list | grep 'sentry-auth-gitlab (0.1.0)'"
end

template File.join(sentry_conf['config_path'], 'sentry.conf.py') do
  owner  'www-data'
  group  'root'
  mode   '0640'
  variables(
      sentry_conf.to_hash()
    )
  notifies :restart, 'service[supervisor]', :delayed
end

template File.join(sentry_conf['config_path'], 'config.yml') do
  owner  'www-data'
  group  'root'
  mode   '0640'
  variables(
      sentry_conf.to_hash()
    )
  notifies :restart, 'service[supervisor]', :delayed
end

node.default['postgresql']['config']['autovacuum_analyze_scale_factor'] = 0.005
node.default['postgresql']['config']['autovacuum_vacuum_cost_delay'] = '5ms'
node.default['postgresql']['config']['autovacuum_vacuum_cost_limit'] = 6000
node.default['postgresql']['config']['autovacuum_vacuum_scale_factor'] = 0.005
node.default['postgresql']['config']['autovacuum_vacuum_threshold'] = 50
node.default['postgresql']['config']['log_autovacuum_min_duration'] = 0
