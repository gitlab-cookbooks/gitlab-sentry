gitlab-sentry Cookbook
======================
This cookbook will install and configure sentry

Attributes
----------
#### gitlab-sentry::default
<table>
  <tr>
    <th>Key</th>
    <th>Type</th>
    <th>Description</th>
    <th>Default</th>
  </tr>
  <tr>
    <td><tt>['gitlab-sentry']['bacon']</tt></td>
    <td>Boolean</td>
    <td>whether to include bacon</td>
    <td><tt>true</tt></td>
  </tr>
</table>

Usage
-----
#### gitlab-sentry::default
Include `gitlab-sentry` in your node's `run_list` and perform the post install listed below:

```json
{
  "name":"my_node",
  "run_list": [
    "recipe[gitlab-sentry]"
  ]
}
```

Post Install
------------
Sentry provides an easy way to run migrations on the database on version upgrades. Before running it for the first time you’ll need to make sure you’ve created the database and user:

```bash
# su - postgres -c 'createdb -E utf-8 sentry'
# su - postgres -c 'createuser -S -D -R -P -e sentry'
# su - postgres -c 'sql'
GRANT ALL ON DATABASE sentry TO sentry;
```

Once done, you can create the initial schema using the upgrade command:
```bash
# SENTRY_CONF=/etc/sentry /usr/share/nginx/sentry/bin/sentry upgrade
```

Next up you’ll need to create the first user, which will act as a superuser:
```bash
# SENTRY_CONF=/etc/sentry /usr/share/nginx/sentry/bin/sentry createuser
```

All schema changes and database upgrades are handled via the upgrade command, and this is the first thing you’ll want to run when upgrading to future versions of Sentry.


Contributing
------------
TODO: (optional) If this is a public cookbook, detail the process for contributing. If this is a private cookbook, remove this section.

e.g.
1. Fork the repository on Github
2. Create a named feature branch (like `add_component_x`)
3. Write your change
4. Write tests for your change (if applicable)
5. Run the tests, ensuring they all pass
6. Submit a Pull Request using Github

License and Authors
-------------------
Authors: TODO: List authors
